using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
	public Text winLose;
	public Text score;

	// Use this for initialization
	void Start ()
	{
		winLose.text = "You Win";
	}
	
	// Update is called once per frame
	void Update ()
	{
		score.text = PlayerPrefs.GetInt ("Score").ToString();
		if (GameOver.isDead.Equals (true)) {
			winLose.text = "You lose";
		}
	}
}

