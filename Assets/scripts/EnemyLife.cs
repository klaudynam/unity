﻿using UnityEngine;
using System.Collections;

public class EnemyLife : MonoBehaviour
{
	public int enemyLife;
	public Material material2, material3;
	private int pointForKill;
	private MeshRenderer rend;
	void Start ()
	{
		rend = GetComponent<MeshRenderer>();
		enemyLife = Random.Range (1, 4);
		pointForKill = 1;
	}

	public void decreseLife()
	{
		enemyLife--;
		if (enemyLife == 2)
			rend.material = material2;
		if(enemyLife==1)
			rend.material = material3;
		if (enemyLife == 0) {
			PlayerController.setPoints (pointForKill);
			Destroy (gameObject);
		} 
	}
}

