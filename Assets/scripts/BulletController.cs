﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

	private Transform bullet;
	public float speed; 

	void Start () {
		bullet = GetComponent<Transform> ();
	}
		
	// Update is called once per frame
	void FixedUpdate () {
		bullet.position += Vector3.up * speed;
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D collider){
		
		if (collider.tag.Equals("Enemy")) {;
			EnemyLife enemyLifeScript = collider.gameObject.GetComponent<EnemyLife>();
			enemyLifeScript.decreseLife ();
			Destroy (this.gameObject);
			}
		}
}
//}
