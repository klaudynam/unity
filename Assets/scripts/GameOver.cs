﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
	private int points; 
	public static bool isDead = false;
	public static bool isGameOver = false;
	void changeScene(){
		SceneManager.LoadSceneAsync ("gameOver", LoadSceneMode.Single );
	}

	void Update(){
		if (isDead.Equals (true)) {
			points = PlayerController.getPoints ();
			PlayerPrefs.SetInt ("Score", points);
			Debug.Log (PlayerPrefs.GetInt ("Score"));
			changeScene ();
		}
		if(isGameOver.Equals(true)){
			points = PlayerController.getPoints ();
			PlayerPrefs.SetInt ("Score", points);
			changeScene ();
		}
	}
}
