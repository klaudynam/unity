﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public Transform bulletSpawn;
	public static int life;
	public float speed, maxX, minX, fireRate;
	public GameObject bullet;
	private Transform playerX;
	private float nextFire;
	public static bool enemyTooLow;
	private static int points;
	public Text lifeTxt;
	public Text pointsTxt; 

	// Use this for initialization
	void Start () {
		playerX = GetComponent<Transform> ();
		life = 3;
		lifeTxt.text= life.ToString ();
		pointsTxt.text =  getPoints().ToString ();
	}

	void FixedUpdate () {
		float x = Input.GetAxis ("Horizontal");
		if (playerX.position.x < minX && x < 0) {
			x = 0;
		} else if (playerX.position.x > maxX && x > 0) {
			x = 0;
		}
		playerX.position += Vector3.right * x * speed;
	}

	// Update is called once per frame
	void Update(){
		if (Input.GetMouseButtonDown (0) && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation);
		}
		Debug.Log (PlayerController.life);
		lifeTxt.text = life.ToString ();
		pointsTxt.text = getPoints().ToString ();
		ifLose ();
	}

	void ifLose()
	{
		if (life <= 0)
			GameOver.isDead = true;
		if (enemyTooLow)
			GameOver.isDead = true;
	}

	public static int getPoints(){
		return points;
	}
	public static void setPoints(int point){
		points+= point;
	}
	public static void restartPoints(){
		points = 0;
	}
}
