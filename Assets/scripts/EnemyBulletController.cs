﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyBulletController : MonoBehaviour
{
	private Transform bullet;
	public float speed; 
	// Use this for initialization
	void Start () {
		bullet = GetComponent<Transform> ();
	}
		
	// Update is called once per frame
	void FixedUpdate () {
		bullet.position -= Vector3.up * speed;
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.tag.Equals("Player")) {
			PlayerController.life--;
			Destroy (gameObject);
		}
	}
}

