﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
	public Transform bulletSpawn;
	public float speed, maxX, minX, maxY, fireRate, horizontalChange, verticalChange;
	public GameObject bullet;
	public static Transform enemies;
	private float nextFire;

	void Start () {
		InvokeRepeating ("MoveEnemy", 0f, horizontalChange);
		enemies = GetComponent<Transform> ();

	}

	float randomFireRate(float min, float max){
		return Random.Range (min, max);
	}

	void MoveEnemy() {
		enemies.position += Vector3.right * speed;
		foreach (Transform enemy in enemies) {
			if (enemy.position.x < minX || enemy.position.x > maxX) {
				speed = -speed;
				enemies.position += Vector3.down * verticalChange;
				return;
			}
			if (enemies.position.y <= maxY) {
				GameOver.isDead = true;
			}
			if (Random.value > fireRate) {
				Instantiate (bullet, enemy.position, enemy.rotation);
			}
		}
		if (enemies.childCount == 0) {
			GameOver.isGameOver = true;
		}
	}
}
