﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Restart : MonoBehaviour {
	public void restart(){
		GameOver.isGameOver = false;
		GameOver.isDead = false;
		PlayerController.restartPoints();
		PlayerController.life = 3;
		changeScene ();
	}

	void changeScene(){
		SceneManager.LoadSceneAsync ("game", LoadSceneMode.Single );
	}
}
